function start_game(){
	var sbet = document.forms["play"]["sbet"].value;
	if(isNaN(sbet) || sbet < 1){
		console.log("fail case");
		alert("Invalid starting bet.");
		return false;
	}
	else{
		localStorage.setItem('sbet', sbet);
		run_game();
	}
}

function run_game(){
	var sbet = parseInt(localStorage.getItem("sbet"));
	var cbet = sbet;
	var hbet = sbet;
	var hbet_roll = 1;
	var roll = 0;
	console.log("||||||||||||||||||GAME START||||||||||||||||||");
	while(cbet > 0){
		roll++;
		
		var d1 = Math.floor(Math.random() * 6) + 1;
		var d2 = Math.floor(Math.random() * 6) + 1;
		
		console.log("ROLL:  = "+ roll);
		console.log("d1 = "+ d1);
		console.log("d2 = "+ d2);
		
		if(d1 + d2 == 7){
			cbet += 4;
			console.log("Rolled 7, current balance is "+ cbet);
		}
		else{
			cbet -= 1;
			console.log("Rolled "+ (d1+d2) +", current balance is "+ cbet);
		}
		
		if(cbet > hbet){
			hbet = cbet;
			hbet_roll = roll;
			console.log("New high score");
		}
	}
	console.log("Game over.");
	
	console.log("hbet: "+hbet);
	console.log("hbet_roll: "+hbet_roll);
	console.log("rolls: "+roll);

	localStorage.setItem('hbet', hbet);
	localStorage.setItem('hbet_roll', hbet_roll);
	localStorage.setItem('roll', roll);	
}

function game_over(){
	document.getElementById("sbet").innerHTML = localStorage.getItem("sbet");
	document.getElementById("roll").innerHTML = localStorage.getItem("roll");
	document.getElementById("hbet").innerHTML = localStorage.getItem("hbet");
	document.getElementById("hbet_roll").innerHTML = localStorage.getItem("hbet_roll");
}